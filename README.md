# tha_audio_list

This node plays all the children AudioStreamPlayer(2D/3D) without pauses between them.

## Usage

Once the addon is enabled, you will be able to add the "AlignedLabel" nodes to your scenes. Add the AudioStreamPlayer(2D/3D).

**Note**: Avoid looping tracks.

## Download

Download: [Zip](https://gitlab.com/TheraotAddons/tha_audio_list/-/jobs/artifacts/deploy/download?job=deploy-job).

## Install

**Option 1**:

- Download the zip.
- Extract it.
- Copy the "addons" folder to your Godot project folder.

**Option 2**:

- Download the zip.
- In Godot, with your project open, go to AssetLib.
- Click "Import..."
- Select the zip file (Godot will show its contents).
- Click "Install".

## Enable

Go to the Menu "Project" -> "Project Settings" -> "Plugins" Tab, there you can enable the addon.

## Branches

- main: The public facing branch.
- addon: Just the addon.
- deploy: The addon plus the continuos integration script.
- project: A Godot project with the addon.
- example: A Godot project with the addon that demonstrates it.

## License
See [LICENSE](https://gitlab.com/TheraotAddons/tha_audio_list/-/blob/main/LICENSE)